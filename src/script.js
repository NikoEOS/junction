import './style.css'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import * as dat from 'dat.gui'

// Debug
const gui = new dat.GUI()

// Canvas
const canvas = document.querySelector('canvas.webgl')
const epoch = new Date('2019-07-02T00:00:00')

// Scene
const scene = new THREE.Scene()

// Objects
const geometry = new THREE.TorusGeometry( .7, .2, 16, 100 );

// Materials

const material = new THREE.MeshBasicMaterial()
material.color = new THREE.Color(0xff0000)

// Mesh
const sphere = new THREE.Mesh(geometry,material)
//scene.add(sphere)

// Lights

const pointLight = new THREE.PointLight(0xffffff, 0.1)
pointLight.position.x = 2
pointLight.position.y = 3
pointLight.position.z = 4
scene.add(pointLight)

/**
 * Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

window.addEventListener('resize', () =>
{
    // Update sizes
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    // Update camera
    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix()

    // Update renderer
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})

/**
 * Camera
 */
// Base camera
const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height, 0.1, 100)
camera.position.x = 0
camera.position.y = 0
camera.position.z = 1
camera.up.set(0,0,1)
scene.add(camera)

gui.add(camera.position, "x").min(-50).max(50)
gui.add(camera.position, "y").min(-50).max(50)
gui.add(camera.position, "z").min(-50).max(50)
gui.add(camera, "far").min(-50).max(50)
gui.add(camera, "near").min(-50).max(50)


// Controls


THREE.MapControls = function ( object, domElement ) {

	OrbitControls.call( this, object, domElement );

	this.mouseButtons.LEFT = THREE.MOUSE.PAN;
	this.mouseButtons.RIGHT = THREE.MOUSE.ROTATE;

	this.touches.ONE = THREE.TOUCH.PAN;
	this.touches.TWO = THREE.TOUCH.DOLLY_ROTATE;

};



THREE.MapControls.prototype = Object.create( THREE.EventDispatcher.prototype );
THREE.MapControls.prototype.constructor = THREE.MapControls;

const controls = new THREE.MapControls(camera, canvas)
controls.enableDamping = true

function scaleX(value) {
  return (value - 380000)/5000
}

function scaleY(value) {
  return (value - 6672500)/5000
}

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas
})
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

const fontLoader = new THREE.FontLoader();


fontLoader.load( 'helvetiker_regular.typeface.json', function ( font ) {
  const material = new THREE.MeshBasicMaterial( {
      color: 0xFFFFFF
  });

  fetch("./names.json")
  .then(response => response.json())
  .then(json => {
    for (let index = 0; index < json.length; index++) {
      const text = json[index][0]
      const pos = json[index][1]
      const x = (pos[0] - 380000)/5000
      const y = (pos[1] - 6672500)/5000
      const geometry = new THREE.TextGeometry( text, {
        font: font,
        size: 0.01,
        height: 0,
        curveSegments: 12,
        bevelEnabled: false,
      } );

      const mesh = new THREE.Mesh( geometry, material )
      /*
      const boundingBox = new THREE.Box3().setFromObject(mesh)
      size = boundingBox.getSize() // Returns Vector3
      console.log(size)
      */
      mesh.position.z = 0.001
      mesh.position.x = x- 0.005
      mesh.position.y = y
      scene.add(mesh)
    }
  });
});

/**
 * Animate
 */


function createPolygon( poly, color ) {
  var shape = new THREE.Shape();
  shape.moveTo( poly[0][0], poly[0][1] );
  for (var i = 1; i < poly.length; ++ i)
      shape.lineTo( poly[i][0], poly[i][1] );
  shape.lineTo( poly[0][0], poly[0][1] );

  var geometry = new THREE.ShapeGeometry( shape );
  var material = new THREE.MeshBasicMaterial( {
      color: color
  } );
  const mesh = new THREE.Mesh(geometry, material);
  return [mesh, material]
}

const cityList = {}

function generateCities(){
  console.log(cities)
  for (let index = 0; index < cities.length; index++) {
    const city = cities[index];
    const cityColor = generateColor(0)
    const postalCode = parseInt(city[0])
    const fragments = city[1]
    const materials = [];
    for (let index = 0; index < fragments.length; index++) {
      const fragment = fragments[index]
      const points = [];
      const poly = [];
      
      for (let index = 0; index < fragment.length; index++) {
        const pos = fragment[index];
        let x = (pos[0] - 380000)/5000
        let y = (pos[1] - 6672500)/5000
      
        points.push(new THREE.Vector3(x,y,0 ));
        poly.push([x,y])
      }
      const cityPoly = createPolygon(poly, cityColor)
      scene.add(cityPoly[0])
      
      materials.push(cityPoly[1])

      const lineMaterial = new THREE.LineBasicMaterial( { color: 0x000000 } );
      const lineGeometry = new THREE.BufferGeometry().setFromPoints( points );
      const line = new THREE.Line( lineGeometry, lineMaterial );
      scene.add( line );
    }
    city.push(generateMasts(masts[postalCode],postalCode))
    city.push(materials)
    city.push(0)
  }
}



function updateCities (movementIndex, tickCount) {
  //console.log(movementIndex, tickCount)
  for (let index = 0; index < cities.length; index++) {
    const materials = cities[index][3]
    const value = cities[index][4] + (colors[movementIndex][index])
    const mastCount = cities[index][2].length
    const activeMastCount = parseInt(mastCount * value)
    //console.log(mastCount, activeMastCount)
    //console.log(value)
    //console.log(movementIndex, colors[movementIndex][index])
    cities[index][4] = value
    //console.log(cities[index][4])
    const color = generateColor(value)
    //console.log(cities[index][4] + colors[movementIndex][index])
    for (let index = 0; index < materials.length; index++) {
      const material = materials[index];
      material.color = new THREE.Color( color );         
      material.needsUpdate = true;
    }
    
    for (let mastIndex = 0; mastIndex < cities[index][2].length; mastIndex++) {
      const material = cities[index][2][mastIndex].material
      if (mastIndex > activeMastCount){
        material.color = new THREE.Color( 0x000000 );   
        material.needsUpdate = true;
      }else{
        material.color = new THREE.Color( 0xffff00 );   
        material.needsUpdate = true;
      }
    }


  }
}

//console.log(cities)
//console.log(masts)
const loader = new THREE.TextureLoader();
const map = loader.load("./Asset_1siniii.png")

function generateMasts(masts, postalCode) {
  if(masts){
    const mastList = []
    for ( let i = 0; i < masts.length; i ++ ) {
      const x = (masts[i][0] - 380000)/5000
      const y = (masts[i][1] - 6672500)/5000
      const z = 0.035
    
      const geometry = new THREE.ConeGeometry( 0.007, 0.07, 4 );
      const material = new THREE.MeshBasicMaterial( {color: 0xffff00} );
      const cone = new THREE.Mesh( geometry, material );
      cone.position.x = x
      cone.position.z = z
      cone.position.y = y
      cone.rotation.x = Math.PI/2
      scene.add( cone );
      mastList.push(cone)
    } 
    return mastList
  }else{
    return []
  }
}

let masts = {}

fetch("./masts.json")
  .then(response => response.json())
  .then(json => {
    masts = json
    generateCities()
  });

let movements = []

fetch("./movements2.json")
  .then(response => response.json())
  .then(json => {
    movements = json
    generateMovement(0)
  });

let colors = []

fetch("./colors.json")
  .then(response => response.json())
  .then(json => {
    colors = json
  });

let cityNames = []



function generateCityNames(cityNames) {

}

let isEverythingFetched = false

function everythingFetched() {
  return (colors.length > 0 && movements.length > 0 && masts != {})
}

let movementList = []

function generateMovement(movementIndex){
  if(movements.length > 0){
    const movement = movements[movementIndex];
    for (let index = 0; index < movement.length; index++) {
      const movementFragment = movement[index]
      const movementsGeometry = new THREE.BufferGeometry();
      const x = scaleX(movementFragment[0][0])
      const y = scaleY(movementFragment[0][1])
      movementsGeometry.setAttribute( 'position', new THREE.Float32BufferAttribute( [x,y,0.025], 3 ) );
      const movementsMaterial = new THREE.PointsMaterial( {
        color: 0xFFFFFF,
        size: 0.07,
        map: map,
        transparent: true,
        opacity: 0
        });
      const movementMesh = new THREE.Points( movementsGeometry, movementsMaterial );
      scene.add( movementMesh );
      movementList.push( movementMesh )
    }
  }
}

function calculateMovement(movementIndex, tickCount){
  let opacity = 1
  if(tickCount < 10){
    opacity = tickCount/10;
  }else if(tickCount > 120){
    opacity = 0
  }else if(tickCount > 110){
    opacity = 1-tickCount/120;
  }else {
    opacity = 1
  }
  
  for (let index = 0; index < movementList.length; index++) {
    const steps = movements[movementIndex][index][1]
    const x = steps[0]/5000
    const y = steps[1]/5000
    movementList[index].translateX(x)
    movementList[index].translateY(y)
    movementList[index].material.opacity = opacity
  }
}

function resetMovement(){
  for (let index = 0; index < movementList.length; index++) {
    scene.remove(movementList[index])
  }
  movementList = []
}


const clock = new THREE.Clock()

let tickCount = 0
let totalTicks = 0
let movementIndex = 0
document.getElementById("day").innerHTML = epoch.toLocaleDateString("fi-FI")


const tick = () => {
    if(isEverythingFetched){
      if(tickCount < 120){
        calculateMovement(movementIndex, tickCount)
        if(colors.length > 0){
          updateCities(movementIndex, tickCount)
        }
      }else if(tickCount == 130){
        resetMovement()
        movementIndex++
        document.getElementById("day").innerHTML = addDays(epoch,movementIndex).toLocaleDateString("fi-FI") + " (Day " + movementIndex + ")"
        if(movementIndex == movements.length){
          movementIndex = 0
        }
        generateMovement(movementIndex)
      }else if (tickCount == 140){
        tickCount = -1
      }
      tickCount++
      totalTicks++
    }else{
      isEverythingFetched = everythingFetched()
    }

    document.getElementById("energy-saved").innerHTML = parseInt(1.34 * totalTicks)
    document.getElementById("euro-saved").innerHTML = parseInt(80 * totalTicks)
    document.getElementById("phone-saved").innerHTML = parseInt(67.080 * totalTicks / 1000)

    //const elapsedTime = clock.getElapsedTime()
    //console.log(elapsedTime)
    // Update objects
    //sphere.rotation.y = .5 * elapsedTime

    // Update Orbital Controls
    controls.update()
    //updateLines()
    // Render
    renderer.render(scene, camera)

    // Call tick again on the next frame
    window.requestAnimationFrame(tick)
    
}

tick()

document.getElementById("salama").setAttribute("src", "/salama.png")
document.getElementById("puhelin").setAttribute("src", "/puhelin.png")