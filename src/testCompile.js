let api = "https://geo.stat.fi/geoserver/wfs?service=WFS&version=2.0.0&request=GetFeature&typeName=postialue:pno_tilasto&outputFormat=json"


fetch(api)
  .then(response => response.json())
  .then(data => {
    console.log(data)
    const cities = []
    data.features.forEach(feature => {
      if(postalCodes.includes(parseInt(feature.properties.postinumeroalue))){
        city = [feature.properties.postinumeroalue]
        fragments = []
        feature.geometry.coordinates.forEach(coor => {
          const fragment = []
          coor.forEach(fragment => {
            const points = [];
            fragment.forEach(pos => {
              //console.log(pos)
              let x = (pos[0] - 380000)/1000
              let y = (pos[1] - 6672500)/1000
              //console.log(x,y)
              fragment.push(pos)
            })
            fragments.push(fragment)

          })
        })
        city.push(fragments)
        cities.push(city)
      }
      
    })
    console.log(cities)
  });