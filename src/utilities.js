function randomColor () {
  let r = Math.floor(Math.random() * 255);
  let g = Math.floor(Math.random() * 255);
  let b = Math.floor(Math.random() * 255);
  return "rgb(" + r + ", " + g + ", "+ b + ")"
}


const H0 = 0
const S0 = 0
const L0 = 30

const H1 = 38
const S1 = 100
const L1 = 50

const HDelta = (H1 - H0)
const SDelta = (S1 - S0)
const LDelta = (L1 - L0)

function generateColor(value) {
  value = Math.max(0, Math.min(1, value))
  let h = parseInt(value * HDelta + H0)
  let s = parseInt(value * SDelta + S0)
  let l = parseInt(value * LDelta + L0)
  return "hsl(" + h + ", " + s + "%, "+ l + "%)"
}

function addDays(date, days) {
  var result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
}